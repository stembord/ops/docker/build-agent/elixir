#!/bin/bash

repo="stembord/build-agent-elixir"

function build_and_push() {
  local tag=$1
  local version=$2
  docker build --build-arg ELIXIR_VERSION=${version} -t ${repo}:${tag} .
  docker tag ${repo}:${tag} ${repo}:${version}
  docker tag ${repo}:${version} ${repo}:latest
  docker push ${repo}:${tag}
  docker push ${repo}:${version}
  docker push ${repo}:latest
}

build_and_push "1.8" "1.8.2"
build_and_push "1.9" "1.9.1"